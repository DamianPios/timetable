//
// Created by wew8 on 01/12/2018.
//

#include "headers/room.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void room_print(Room *pointer) {
    //get first element, head is always null
    Room *current = pointer->next;
    //check if it is not the end of the list
    while (current) {
        printf("Room type: %c\nCapacity %d\n\n", current->room_type, current->amount);
        //go to next node
        current = current->next;
    }
}


Room *read_room_file(char *directory) {
    //create head of module linked list
    Room *room_head;
    //reserve room in memory for next modules_code
    room_head = malloc(sizeof(Room));
    //pointer to file address
    FILE *fp_rooms;

    char filePath[100];
    strcpy(filePath, directory);
    strcat(filePath, "/rooms.txt");

    //open the file
    fp_rooms = fopen(filePath, "r");
    //length of the longest string
    char str_modules[20];
    //print error if the address of the file does not exist
    if (fp_rooms == NULL) {
        printf("Error loading rooms.txt\n");
        fflush(fp_rooms);
        return 0;
    }

    Room *current = room_head;

    // read line by line data and parse it
    while (fgets(str_modules, 20, fp_rooms)) {
        current->next = malloc(sizeof(Room));
        current = current->next;
        char *tmp = strtok(str_modules, " ");

        current->room_type = *tmp;

        tmp = strtok(NULL, " ");
        current->amount = strtol(tmp, NULL, 0);
    }

    current->next = 0;

    fclose(fp_rooms);

    return room_head;
}