#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "headers/room.h"
#include "headers/time.h"
#include "headers/module.h"
#include "headers/scheme.h"

int main(int argc, char *args[]) {
    int defaultModuleLength = -1;
    if (argc > 1)
        if (strcmp(args[1], "basic") == 0){
            defaultModuleLength = 1;
        }

    char directory[85];
    printf("Please, write path to folder without \"/\" at the end\n");
    gets(directory);

    Module *pModules = read_module_file(directory);

    Scheme *pScheme = read_scheme_file(directory);

    Room *pRoom = read_room_file(directory);

    Time *pTime = read_time_file(directory, pRoom);

    if (pModules == NULL || pScheme == NULL || pRoom == NULL || pTime == NULL) {
        printf("Error while loading data");
        return -1;
    }

    // prepare data to next steps (collect clashed modules + get number of students for each module in list)
    Module *current = pModules->next;
    while (current != NULL) {
        findAllClashedModulesAndCountStudents(pScheme, current);
        current = current->next;
    }

    char moduleName[10];
    printf("Please, write name of module\n");
    gets(moduleName);

    // looking for module and printing appropriate information
    Module *foundModule = findModule(pModules, moduleName);
    if (foundModule == NULL) {
        printf("module %s not found\n", moduleName);
    } else {
        printf("students on this module %d\n", foundModule->students);
        printf("clash modules for:%s\n", moduleName);
        printModuleNameList(foundModule->clashedModules);
    }

    int semester;
    printf("Please, write semester\n");
    char semesterTmp[1];
    gets(semesterTmp);
    semester = strtol(semesterTmp, 0, 0);
    if (semester == 1 || semester == 2) {
        createTimeTable(pModules, pTime, semester, defaultModuleLength);
        printTimetable(pTime);
    } else {
        printf("Wrong semester\n");
        return -1;
    }

    return 0;
}