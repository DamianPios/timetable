


//
// Created by Joanna on 01/12/2018.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "headers/time.h"
#include "headers/module.h"


void print_time(Time *pointer) {
    // get first element, head is always null
    Time *current = pointer->next;
    //check if it is not the end of the list
    while (current) {
        printf("Day: %s\nLectures per day: %i\n", current->day, current->lectures_per_day);
        for (int i = 0; i < current->lectures_per_day; i++) {
            printf("Times: %d\n", current->hours[i]->hour);
        }
        //go to next node
        current = current->next;
    }
}

Time *read_time_file(char *directory, Room *rooms) {
    //create head of module linked list
    Time *time_head;
    //reserve room in memory for next modules_code
    time_head = malloc(sizeof(Time));
    //pointer to file address
    FILE *fp_times;

    char filePath[100];
    strcpy(filePath, directory);
    strcat(filePath, "/times.txt");

    //open the file
    fp_times = fopen(filePath, "r");
    //length of the longest string
    char str_times[40];
    //print error if the address of the file does not exist
    if (fp_times == NULL) {
        printf("Error loading times.txt\n");
        fflush(fp_times);
        return 0;
    }

    Time *current = time_head;

    // read line by line and parse data
    while (fgets(str_times, 40, fp_times) != NULL) {
        current->next = malloc(sizeof(Time));
        current = current->next;

        char *tmp = strtok(str_times, " ");
        strcpy(current->day, tmp);

        tmp = strtok(NULL, " ");
        current->lectures_per_day = strtol(tmp, NULL, 0);

        for (int i = 0; i < current->lectures_per_day; i++) {
            tmp = strtok(NULL, " ");

            int hour = strtol(tmp, NULL, 0);

            current->hours[i] = malloc(sizeof(Class_time));
            current->hours[i]->hour = hour;
            Room *room = rooms->next;
            while (room) {
                if (room->room_type == 'L')
                    current->hours[i]->lectureAmount = room->amount;
                else if (room->room_type == 'P')
                    current->hours[i]->practicalAmount = room->amount;
                room = room->next;
            }
            current->hours[i]->reservedLecture = 0;
            current->hours[i]->reservedPractical = 0;
            current->hours[i]->lectureList = malloc(sizeof(Module));
            current->hours[i]->lectureList->next = 0;
            current->hours[i]->practicalList = malloc(sizeof(Module));
            current->hours[i]->practicalList->next = 0;
        }
    }

    current->next = 0;
    fclose(fp_times);

    return time_head;
}

void createTimeTable(Module *modules, Time *week, int semester, int defaultModuleLength) {
    int allLecturesAssigned = 0;

    // we can reserved only one occurrence of module lecture or practical
    // so we need this loop in case that any module can be more than one a week
    while (allLecturesAssigned == 0) {
        allLecturesAssigned = 1;

        Module *currentModule = modules->next;
        // iterate over list with modules
        while (currentModule != NULL) {
            // filter by semester and with students amount more then 0
            if (currentModule->semester == semester && currentModule->students > 0) {
                int lecturesAdded = 0;
                Time *currentDay = week->next;

                // looking for a day
                while (currentDay != NULL) {
                    // it can happen that modules reserve space for lecture but not for practicals (that day) and that's why we have to check this flag
                    int moduleLength;
                    // here we set length of module (if we want to have fixed length we need to set positive value)
                    // otherwise we get real value from file
                    if (defaultModuleLength >= 0)
                        moduleLength = defaultModuleLength;
                    else
                        moduleLength = currentModule->lectures_hours;

                    // looking for space in day
                    int startOfLecture = findTimeForLectures(currentDay->hours, currentDay->lectures_per_day,
                                                             moduleLength, currentModule->code,
                                                             modules);
                    // if current module have at least one lecture per week and current day have space for module lecture - reserved it, and sets all flags for steering
                    if (currentModule->lectures_per_week > 0 &&
                        currentModule->lecturesAssigned < currentModule->lectures_per_week &&
                        startOfLecture > -1) {
                        // reserving, loop is necessary for correctly reserve amount of hours
                        for (int i = 0; i < moduleLength; i++) {
                            Class_time *hourToReserve = currentDay->hours[startOfLecture + i];
                            appendModuleNameToList(hourToReserve->lectureList, currentModule->code);
                            hourToReserve->reservedLecture++;
                        }
                        // flag that we already reserved lectures, in case that practicals will be reserved other day (reserving lecture will be skipped)
                        lecturesAdded = 1;
                        // increase amount of lectures reserved for this module (so we can correctly control reserving space for lecture during week)
                        currentModule->lecturesAssigned++;
                    } else if (currentModule->lectures_per_week == 0 ||
                               currentModule->lecturesAssigned == currentModule->lectures_per_week)
                        lecturesAdded = 1;
                    // it can happen that modules reserve space for lecture but not for practicals (that day) and that's why we have to check this flag
                    // here we have same algorithm as lectures
                    if (lecturesAdded == 1)
                        break;

                    currentDay = currentDay->next;
                }
                // if next day is null means that we reached sunday, and we need to report error and return what we reached
                if (currentDay == NULL) {
                    break;
                }
                // here we check if all classes of module are reserved loop 'while (allLecturesAssigned == 0)' will run again
                // if at least one module won't be fully reserved
                if (currentModule->lecturesAssigned != currentModule->lectures_per_week)
                    allLecturesAssigned = 0;
            }

            currentModule = currentModule->next;
        }
    }

    int allPracticalsAssigned = 0;
    while (allPracticalsAssigned == 0) {
        allPracticalsAssigned = 1;

        Module *currentModule = modules->next;
        // iterate over list with modules
        while (currentModule != NULL) {
            // filter by semester and with students amount more then 0
            if (currentModule->semester == semester && currentModule->students > 0) {
                int practicalsAdded = 0;
                Time *currentDay = week->next;

                // looking for a day
                while (currentDay != NULL) {
                    // it can happen that modules reserve space for lecture but not for practicals (that day) and that's why we have to check this flag
                    // here we have same algorithm as lectures
                    int moduleLength;
                    if (defaultModuleLength >= 0)
                        moduleLength = defaultModuleLength;
                    else
                        moduleLength = currentModule->practicals_hours;

                    int startOfPractical = findTimeForPracticals(currentDay->hours, currentDay->lectures_per_day,
                                                                 moduleLength,
                                                                 currentModule->code,
                                                                 modules);
                    if (currentModule->practicals_per_week > 0 &&
                        currentModule->practicalAssigned < currentModule->practicals_per_week &&
                        startOfPractical > -1) {
                        for (int i = 0; i < moduleLength; i++) {
                            Class_time *hourToReserve = currentDay->hours[startOfPractical + i];
                            appendModuleNameToList(hourToReserve->practicalList, currentModule->code);
                            hourToReserve->reservedPractical++;
                        }
                        practicalsAdded = 1;
                        currentModule->practicalAssigned++;
                    } else if (currentModule->practicals_per_week == 0 ||
                               currentModule->practicalAssigned == currentModule->practicals_per_week)
                        practicalsAdded = 1;
                    if (practicalsAdded == 1)
                        break;

                    currentDay = currentDay->next;
                }
                // if next day is null means that we reached sunday, and we need to report error and return what we reached
                if (currentDay == NULL) {
                    break;
                }
                // here we check if all classes of module are reserved loop 'while (allPracticalsAssigned == 0)' will run again
                // if at least one module won't be fully reserved
                if (currentModule->practicalAssigned != currentModule->practicals_per_week)
                    allPracticalsAssigned = 0;
            }

            currentModule = currentModule->next;
        }
    }
    findAndPrintNotAssignedModules(modules, semester);
}

int
findTimeForLectures(Class_time *hours[9], int lecturesPerDay, int lectureLength, char *moduleName, Module *moduleList) {
    // iterate over whole day
    for (int i = 0; i < lecturesPerDay; i++) {
        int isHourFree = 0;
        // additional iterate - here we iterate over length of module
        for (int j = 0; j < lectureLength; j++) {
            // if we reach hour + lengthOfModule > lengthOfDay we need to report that in that day are no space for this lecture
            if (i + j >= lecturesPerDay)
                isHourFree = 0;
            else {
                Class_time *hourToCheck = hours[i + j];
                // check if there is room (in this case lecture room) free
                if (hourToCheck->reservedLecture == hourToCheck->lectureAmount) {
                    isHourFree = 0;
                } else {
                    ModuleNameList *actualLecture = hourToCheck->lectureList->next;
                    if (actualLecture == NULL) {
                        isHourFree = 1;
                    }
                    // here we have to check if there is  no clashed modules reserved for this hour
                    while (actualLecture != NULL) {
                        if (actualLecture->name == NULL ||
                            checkIfClash(findModule(moduleList, actualLecture->name)->clashedModules, moduleName) == 0)
                            isHourFree = 1;
                        else {
                            isHourFree = 0;
                            break;
                        }
                        actualLecture = actualLecture->next;
                    }
                }
            }
            if (isHourFree == 0)
                break;
        }
        if (isHourFree == 1)
            return i;
    }
    return -1;
}

// this function works in the same way as 'findTimeForLectures' but for practicals
int findTimeForPracticals(Class_time *hours[9], int practicalsPerDay, int practicalLength, char *moduleName,
                          Module *moduleList) {
    for (int i = 0; i < practicalsPerDay; i++) {
        int isHourFree = 0;
        for (int j = 0; j < practicalLength; j++) {
            if (i + j >= practicalsPerDay)
                isHourFree = 0;
            else {
                Class_time *hourToCheck = hours[i + j];
                if (hourToCheck->reservedPractical == hourToCheck->practicalAmount) {
                    isHourFree = 0;
                } else {
                    ModuleNameList *actualPractical = hourToCheck->practicalList->next;
                    if (actualPractical == NULL) {
                        isHourFree = 1;
                    }
                    while (actualPractical != NULL) {
                        if (actualPractical->name == NULL ||
                            checkIfClash(findModule(moduleList, actualPractical->name)->clashedModules,
                                         moduleName) ==
                            0)
                            isHourFree = 1;
                        else
                            isHourFree = 0;
                        actualPractical = actualPractical->next;
                    }
                }
            }
            if (isHourFree == 0)
                break;
        }
        if (isHourFree == 1)
            return i;
    }
    return -1;
}

// appends module to list
void appendModuleNameToList(ModuleNameList *list, char *moduleName) {
    if (list->next == NULL) {
        list->next = malloc(sizeof(ModuleNameList));
        list->next->next = 0;
        list->next->name = 0;
    }

    ModuleNameList *current = list->next;
    while (current->next != NULL) {
        current = current->next;
    }

    current->name = moduleName;
    current->next = malloc(sizeof(ModuleNameList));
    current->next->name = 0;
    current->next->next = 0;
}

void printTimetable(Time *week) {
    // we have to find longest day (hours)
    int longestDayHours = 0;
    Time *longestDay = 0;
    Time *current = week->next;
    while (current != NULL) {
        if (current->lectures_per_day > longestDayHours) {
            longestDayHours = current->lectures_per_day;
            longestDay = current;
        }
        current = current->next;
    }

    int stringLength = 115;
    Time *currentDay = week->next;

    if (longestDay == NULL) {
        printf("There's no data to display");
        return;
    }

    // print hours(9-17)
    printf("\t\t");
    for (int i = 0; i < longestDayHours; i++) {
        printf("|%d", longestDay->hours[i]->hour);
        if (longestDay->hours[i]->hour < 10)
            for (int j = 1; j < 10; j++) {
                printf(" ");
            }
        if (longestDay->hours[i]->hour >= 10)
            for (int j = 2; j < 10; j++) {
                printf(" ");
            }
    }

    // art accent
    printf("\n");
    for (int i = 0; i < stringLength; i++) {
        printf("=");
    }
    printf("\n");

    // here we print whole day (lectures than practicals)
    while (currentDay != NULL) {
        // print day name with trailing spaces
        char *stretchDayName = "";
        if (strlen(currentDay->day) < 8)
            stretchDayName = "\t";
        printf("%s\t%s", currentDay->day, stretchDayName);

        printf("|Lectures");

        // hardest part
        int counter = 0;
        // here we print all lectures
        // each time we print line, counter is incremented
        // so we know that in next step we have to print next element of list
        // e.g. print first el of monday, print first el of tuesday ... couter++
        // counter == 1 so we know that we have to print second element of each day and so on
        while (counter < longestDay->hours[0]->lectureAmount) {
            printf("\n\t\t");
            for (int i = 0; i < longestDayHours; i++) {
                if (i >= currentDay->lectures_per_day) {
                    printf("        ");
                } else {
                    ModuleNameList *currentLecture = currentDay->hours[i]->lectureList->next;
                    int localCounter = 0;
                    while (currentLecture != NULL && localCounter != counter) {
                        currentLecture = currentLecture->next;
                        localCounter++;
                    }
                    if (currentLecture != NULL && currentLecture->name != NULL) {
                        printf("|%s   ", currentLecture->name);
                    } else
                        printf("\t");
                }
            }
            counter++;
        }

        // art accent
        printf("\n\t\t");
        for (int i = 0; i < stringLength - 16; i++) {
            printf("~");
        }

        printf("\n\t\t");
        printf("|Practicals");
        counter = 0;
        // here we print practicals, but we use same alghorithm as for lectures
        while (counter < longestDay->hours[0]->practicalAmount) {
            printf("\n\t\t");
            for (int i = 0; i < longestDayHours; i++) {
                if (i >= currentDay->lectures_per_day) {
                    printf("        ");
                } else {
                    ModuleNameList *currentPractical = currentDay->hours[i]->practicalList->next;
                    int localCounter = 0;
                    while (currentPractical != NULL && localCounter != counter) {
                        currentPractical = currentPractical->next;
                        localCounter++;
                    }
                    if (currentPractical != NULL && currentPractical->name != NULL) {
                        printf("|%s   ", currentPractical->name);
                    } else
                        printf("\t");
                }
            }
            counter++;
        }

        // art accent
        printf("\n");
        for (int i = 0; i < stringLength; i++) {
            printf("=");
        }
        printf("\n");
        currentDay = currentDay->next;
    }
}

void findAndPrintNotAssignedModules(Module *modulesList, int semester) {
    Module *currentModule = modulesList->next;

    ModuleNameList *practicals = malloc(sizeof(ModuleNameList));
    practicals->next = malloc(sizeof(ModuleNameList));
    ModuleNameList *lastPractical = practicals->next;
    lastPractical->name = 0;

    ModuleNameList *lectures = malloc(sizeof(ModuleNameList));
    lectures->next = malloc(sizeof(ModuleNameList));
    ModuleNameList *lastLecture = lectures->next;
    lastLecture->name = 0;

    while (currentModule != NULL) {
        if (currentModule->semester == semester && currentModule->students > 0) {
            if (currentModule->practicalAssigned < currentModule->practicals_per_week) {
                lastPractical->name = currentModule->code;
                lastPractical->next = malloc(sizeof(ModuleNameList));
                lastPractical = lastPractical->next;
                lastPractical->name = 0;
            }

            if (currentModule->lecturesAssigned < currentModule->lectures_per_week) {
                lastLecture->name = currentModule->code;
                lastLecture->next = malloc(sizeof(ModuleNameList));
                lastLecture = lastLecture->next;
                lastLecture->name = 0;
            }
        }

        currentModule = currentModule->next;
    }

    if (lectures->next->name != NULL || practicals->next->name != NULL)
        printf("Cannot create timetable\n");

    if (lectures->next->name != NULL) {
        printf("Not assigned lectures:\n");
        printModuleNameList(lectures);
    }

    if (practicals->next->name != NULL) {
        printf("\nNot assigned practicals:\n");
        printModuleNameList(practicals);
    }
}