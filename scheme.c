
//
// Created by Joanna on 01/12/2018.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "headers/scheme.h"

/**
 * print data from linked list
 * @param pointer pointer to current node
 */
void print_scheme(Scheme *pointer) {
    // get first element, head is always null
    Scheme *current = pointer->next;
    //check if it is not the end of the list
    while (current) {
        printf("Code: %s\nYear: %d\nNumber of students: %d\nCore module: %d\n", current->code, current->study_year,
               current->students_number, current->core_modules_no);

        for (int i = 0; i < current->core_modules_no; i++) {
            printf("Code: %s\n", current->modules_code[i]);
        }
        current = current->next;
    }
}

Scheme *read_scheme_file(char *directory) {
    //create head of Scheme linked list
    Scheme *scheme_head = NULL;
    //reserve room in memory for next modules_code
    scheme_head = malloc(sizeof(Scheme));
    //pointer to file address
    FILE *fp_schemes;

    char filePath[100];
    strcpy(filePath, directory);
    strcat(filePath, "/schemes.txt");

    //open the file
    fp_schemes = fopen(filePath, "r");
    //length of the longest string
    char str_schemes[95];
    //print error if the address of the file does not exist
    if (fp_schemes == NULL) {
        printf("Error loading schemes.txt\n");
        fflush(fp_schemes);
        return 0;
    }

    Scheme *current = scheme_head;

    // parsing and storing data
    // read line by line
    while (fgets(str_schemes, 95, fp_schemes) != NULL) {
        current->next = malloc(sizeof(Scheme));
        current = current->next;

        char *tmp = strtok(str_schemes, " ");
        strcpy(current->code, tmp);

        tmp = strtok(NULL, " ");
        current->study_year = strtol(tmp, NULL, 0);

        tmp = strtok(NULL, " ");
        current->students_number = strtol(tmp, NULL, 0);

        tmp = strtok(NULL, " ");
        current->core_modules_no = strtol(tmp, NULL, 0);

        for (int i = 0; i < 10; i++) {
            tmp = strtok(NULL, " ");
            char *pos;
            if ((pos = strchr(tmp, '\n')) != NULL)
                *pos = '\0';

            strcpy(current->modules_code[i], tmp);
        }
    }

    current->next = 0;

    fclose(fp_schemes);

    return scheme_head;
}

// collect all clashed modules with module passed as parameter
// additionally - count how many students attends
void findAllClashedModulesAndCountStudents(Scheme *schemaList, Module *module) {
    ModuleNameList *modulesList = calloc(sizeof(ModuleNameList), 0);
    modulesList->next = malloc(sizeof(ModuleNameList));
    modulesList->next->name = module->code;
    modulesList->next->next = 0;

    char *moduleName = module->code;

    int studentsNo = 0;
    Scheme *currentScheme = schemaList->next;

    // loop scheme list
    while (currentScheme) {
        int found = 0;
        // looking for module on scheme's modules list
        for (int i = 0; i < currentScheme->core_modules_no; i++) {
            if (strcmp(currentScheme->modules_code[i], moduleName) == 0) {
                found = 1;
                break;
            }
        }

        // if module is found in scheme module list
        if (found == 1) {
            // add number of students from scheme
            studentsNo += currentScheme->students_number;
            // loop over scheme's modules list
            for (int i = 0; i < currentScheme->core_modules_no; i++) {
                // module code from scheme's modules list
                char *currentModuleCode = currentScheme->modules_code[i];
                // if currentModuleCode is not equal name of module we passed as argument
                if (strcmp(currentModuleCode, moduleName) != 0) {
                    // we need to check if module code already exists in our list
                    // so here is start of list
                    ModuleNameList *current = modulesList->next;

                    // in case of first attempt, where list is empty
                    if (current->name == NULL) {
                        current->name = currentModuleCode;
                        current->next = 0;
                    }

                    // loop through clashed module list
                    // we need this loop to filter repeating modules (we'd like to have only unique names)
                    while (current != NULL) {
                        // if module name already exist on list
                        if (current->name != NULL && strcmp(current->name, currentModuleCode) == 0) {
                            break;
                        }
                        // if we reached end of list - it means that module not exist on list
                        else if (!current->next) {
                            current->next = malloc(sizeof(ModuleNameList));
                            current->next->name = currentModuleCode;
                            current->next->next = 0;
                            break;
                        }
                        // if we're not at the end of list, and module name was not found on already processed elements
                        else {
                            current = current->next;
                        }
                    }
                }
            }
        }
        // look for another scheme
        currentScheme = currentScheme->next;
    }
    // store all collected data in module passed as function argument
    module->clashedModules = modulesList;
    module->students = studentsNo;
}
