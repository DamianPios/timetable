//
// Created by Joanna on 01/12/2018.
//

#ifndef TIMETABLE_MODULE_H
#define TIMETABLE_MODULE_H

typedef struct module_name_list {
    char *name;
    struct module_name_list *next;
}ModuleNameList;

typedef struct module{
    char code[7];
    int semester;
    int students;
    int lectures_per_week;
    int lectures_hours;
    int practicals_per_week;
    int practicals_hours;
    int lecturesAssigned;
    int practicalAssigned;
    struct module_name_list *clashedModules;
    struct module *next;
}Module;

/**
 * prints data read from file
 * @param pointer to data
 */
void print_module(Module *pointer);

/**
 * reads data from file
 * @param directory where files is stored
 * @return data as linked list
 */
Module* read_module_file(char *directory);

/**
 * looks for module of given name
 * @param moduleList list of all modules
 * @param moduleName searches module
 * @return module or null
 */
Module *findModule(Module *moduleList, char *moduleName);

/**
 * prints all elements from list
 * @param list
 */
void printModuleNameList(ModuleNameList *list);

/**
 * checks if candidateModule is not on clashed list of module
 * @param module with clashed list
 * @param candidateModule name of candidate module to check
 * @return
 */
int checkIfClash(ModuleNameList *module, char *candidateModule);
#endif //TIMETABLE_MODULE_H