//
// Created by wew8 on 01/12/2018.
//
#ifndef TIMETABLE_ROOM_H
#define TIMETABLE_ROOM_H

typedef struct room{
    char room_type;
    int amount;
    struct room *next;
}Room;

/**
 * prints data
 * @param pointer to list with data
 */
void room_print(Room *pointer);

/**
 *
 * @param directory where file is stored
 * @return data as linked list
 */
Room* read_room_file(char *directory);
#endif //TIMETABLE_ROOM_H