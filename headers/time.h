//
// Created by Joanna on 01/12/2018.
//

#ifndef TIMETABLE_TIME_H
#define TIMETABLE_TIME_H

#include "room.h"
#include "module.h"

typedef struct class_time {
    int hour;
    int lectureAmount;
    int practicalAmount;
    int reservedLecture;
    int reservedPractical;
    ModuleNameList *lectureList;
    ModuleNameList *practicalList;
} Class_time;

typedef struct times {
    int lectures_per_day;
    struct times *next;
    char day[10];
    Class_time *hours[9];
} Time;

/**
 * prints data read from file
 * @param pointer to data
 */
void print_time(Time *pointer);

/**
 *
 * @param directory directory where file is stored
 * @param rooms list of available rooms, we need to know how many modules we can store on each hour
 * @return list with days
 */
Time *read_time_file(char *directory, Room *rooms);

/**
 *
 * @param modules list of all modules
 * @param week representation of week (days, and hours)
 * @param semester semester 1 or 2
 * @param defaultModuleLength we can specify fixed length of module, if value is negative, real value are used
 */
void createTimeTable(Module *modules, Time *week, int semester, int defaultModuleLength);

/**
 * prints formatted week
 * @param week list of days with data to print
 */
void printTimetable(Time *week);

/**
 * we need to find room for lectures during day
 * @param hours array with hours of current day
 * @param lecturesPerDay how many lectures can be that day
 * @param lectureLength how long is lecture
 * @param moduleName
 * @param moduleList list with all modules
 * @return
 */
int findTimeForLectures(Class_time *hours[9], int lecturesPerDay, int lectureLength, char *moduleName,
                        Module *moduleList);

/**
 * we need to find room for practicals during day
 * @param hours array with hours of current day
 * @param practicalsPerDay how many practicals can be that day
 * @param practicalLength how long is practical
 * @param moduleName
 * @param moduleList list with all modules
 * @return
 */
int findTimeForPracticals(Class_time *hours[9], int practicalsPerDay, int practicalLength, char *moduleName,
                          Module *moduleList);
/**
 * creates list of module's names
 * @param list list where name will be appended
 * @param moduleName name to append
 */
void appendModuleNameToList(ModuleNameList *list, char *moduleName);

/**
 * finds and prints modules that cannot be placed on timetable
 * works only after run createTimeTable
 * @param modulesList all modules
 * @param semester
 */
void findAndPrintNotAssignedModules(Module *modulesList, int semester);

#endif //TIMETABLE_TIME_H
