//
// Created by Joanna on 01/12/2018.
//

#ifndef TIMETABLE_SCHEME_H
#define TIMETABLE_SCHEME_H

#include "module.h"

typedef struct scheme{
    char code[5];
    int study_year;
    int students_number;
    int core_modules_no;
    struct scheme *next;
    char modules_code[10][9];
}Scheme;

/**
 * prints data
 * @param pointer to data
 */
void print_scheme(Scheme *pointer);

/**
 *
 * @param directory where file is stored
 * @return data as linked list
 */
Scheme* read_scheme_file(char *directory);

/**
 * looking for all clashed modules for module
 * @param schemaList all schemes
 * @param module module for which we want to find clashed modules
 */
void findAllClashedModulesAndCountStudents(Scheme *schemaList, Module *module);
#endif //TIMETABLE_SCHEME_H
