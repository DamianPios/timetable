//
// Created by Joanna on 01/12/2018.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "headers/module.h"
#include "headers/scheme.h"

/**
 * print data from linked list
 * @param pointer pointer to current node
 */
void print_module(Module *pointer) {
    //get first element, head is always null
    Module *current = pointer->next;
    //check if it is not the end of the list
    while (current) {
        printf("Code: %s\nSemester: %d\nLecture hours:\nper week: %d hours: %d\nPractical hours:\nper week: %d hours: %d\n\n",
               current->code, current->semester,
               current->lectures_per_week, current->lectures_hours, current->practicals_per_week,
               current->practicals_hours);
        //go to next node
        current = current->next;
    }
}

Module *read_module_file(char *directory) {
    //create head of module linked list
    Module *module_head;
    //reserve room in memory for next modules_code
    module_head = malloc(sizeof(Module));
    //pointer to file address
    FILE *fp_modules;
    //open the file

    char filePath[100];
    strcpy(filePath, directory);
    strcat(filePath, "/modules.txt");

    fp_modules = fopen(filePath, "r");
    //length of the longest string
    char str_modules[20];
    //print error if the address of the file does not exist
    if (fp_modules == NULL) {
        printf("Error loading modules.txt\n");
        fflush(fp_modules);
        return 0;
    }

    Module *current = module_head;

    while (fgets(str_modules, 20, fp_modules) != NULL) {
        current->next = malloc(sizeof(Module));
        current = current->next;

        char *tmp = strtok(str_modules, " ");
        strcpy(current->code, tmp);

        tmp = strtok(NULL, " ");
        current->semester = strtol(tmp, NULL, 0);;

        tmp = strtok(NULL, " ");
        char lectures[strlen(tmp)];
        strcpy(lectures, tmp);

        tmp = strtok(NULL, " ");
        current->practicals_per_week = strtol(strtok(tmp, "P"), NULL, 0);
        current->practicals_hours = strtol(strtok(NULL, "P"), NULL, 0);

        current->lectures_per_week = strtol(strtok(lectures, "L"), NULL, 0);
        current->lectures_hours = strtol(strtok(NULL, "L"), NULL, 0);

        current->lecturesAssigned = 0;
        current->practicalAssigned = 0;
    }

    current->next = 0;

    fclose(fp_modules);

    return module_head;
}

Module *findModule(Module *moduleList, char *moduleName) {
    Module *current = moduleList->next;

    while (current != NULL) {
        if (strcmp(current->code, moduleName) == 0) {
            return current;
        }
        current = current->next;
    }

    return NULL;
}

void printModuleNameList(ModuleNameList *list) {
    ModuleNameList *current = list->next;
    while (current != NULL && current->name != NULL) {
        printf("%s\n", current->name);
        current = current->next;
    }
}

int checkIfClash(ModuleNameList *module, char *candidateModule) {
    ModuleNameList *currentClashedModule = module->next;
    while (currentClashedModule != NULL) {
        if (currentClashedModule->name != NULL && strcmp(currentClashedModule->name, candidateModule) == 0)
            return 1;
        currentClashedModule = currentClashedModule->next;
    }
    return 0;
}

